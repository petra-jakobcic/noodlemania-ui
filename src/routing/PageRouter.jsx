import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import HomePage from "../pages/HomePage";
import MenuPage from "../pages/MenuPage";
import ProductPage from "../pages/ProductPage";
import AboutUsPage from "../pages/AboutUsPage";
import ContactUsPage from "../pages/ContactUsPage";
import BasketPage from "../pages/BasketPage";
import CheckoutPage from "../pages/CheckoutPage";

export default function PageRouter() {
  return (
    <Router>
      <Routes>
        {/* Remember to use "exact" if the beginning of the path is used in other routes. */}
        <Route path="/" exact element={<HomePage />} />
        <Route path="/menu" exact element={<MenuPage />} />
        <Route path="/menu/:slug" element={<ProductPage />} />
        <Route path="/about-us" element={<AboutUsPage />} />
        <Route path="/contact-us" element={<ContactUsPage />} />
        <Route path="/basket" element={<BasketPage />} />
        <Route path="/checkout" element={<CheckoutPage />} />
      </Routes>
    </Router>
  );
}
