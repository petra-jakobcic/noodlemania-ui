import PropTypes from "prop-types";

export default function InputWithLabel({
  id,
  label,
  value,
  setValue,
  disabled,
  placeholder
}) {
  const handleInputChange = e => setValue(e.target.value);

  return (
    <div className="form__item u-margin-bottom-small">
      <label htmlFor={id}>{label}</label>

      <input
        className="form__input"
        type="text"
        id={id}
        value={value}
        onChange={handleInputChange}
        disabled={disabled}
        placeholder={placeholder}
      />
    </div>
  );
}

InputWithLabel.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  setValue: PropTypes.func.isRequired,
  placeholder: PropTypes.string
};
