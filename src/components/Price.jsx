/**
 * Adds a dot to the formatted number of pence expressed in pounds.
 *
 * @param {number} pence The amount of pennies.
 *
 * @returns {string} The formatted number of pence as pounds.
 */
const penceToPounds = pence => {
  // Pad the price with zeros, if needed.
  const paddedPence = pence.toString().padStart(3, "0");

  // Add the dot two spaces before the end of the string.
  return (
    paddedPence.substr(0, paddedPence.length - 2) + "." + paddedPence.substr(-2)
  );
};

export default function Price({ amount, quantity = 1 }) {
  const price = penceToPounds(amount * quantity);

  return <span>£{price}</span>;
}
