import { Link } from "react-router-dom";
import { FaHome, FaUtensils, FaInfoCircle, FaEnvelope } from "react-icons/fa";
import BasketWithSize from "./BasketWithSize";

export default function Navigation() {
  return (
    <nav className="nav">
      <ul className="nav__links">
        <li>
          <Link to="/" className="nav__link">
            <FaHome className="nav__icon" />
            <span className="nav__text">Home</span>
          </Link>
        </li>

        <li>
          <Link to="/menu" className="nav__link">
            <FaUtensils className="nav__icon" />
            <span className="nav__text">Menu</span>
          </Link>
        </li>

        <li>
          <Link to="/contact-us" className="nav__link">
            <FaEnvelope className="nav__icon" />
            <span className="nav__text">Contact Us</span>
          </Link>
        </li>

        <li>
          <Link to="/about-us" className="nav__link">
            <FaInfoCircle className="nav__icon" />
            <span className="nav__text">About Us</span>
          </Link>
        </li>

        <BasketWithSize />
      </ul>
    </nav>
  );
}
