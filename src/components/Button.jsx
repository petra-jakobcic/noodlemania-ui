export default function Button({ caption, action }) {
  /**
   * Handles the click of a button.
   *
   * @returns {void}
   */
  const handleButtonClick = () => {
    action();
  };

  return (
    <button className="button menu__button" onClick={handleButtonClick}>
      {caption}
    </button>
  );
}
