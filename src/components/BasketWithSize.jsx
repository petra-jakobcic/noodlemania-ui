import { useContext } from "react";
import { BasketContext } from "../contexts/BasketContext";
import { Link } from "react-router-dom";
import { FaShoppingCart } from "react-icons/fa";

export default function BasketWithSize() {
  const { getBasketSize } = useContext(BasketContext);

  return (
    <li className="basket">
      <span>
        <Link to="/basket" className="nav__link">
          <FaShoppingCart />
        </Link>
      </span>

      <span className="basket__size">{getBasketSize()}</span>
    </li>
  );
}
