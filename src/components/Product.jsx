import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Button from "./Button";
import Price from "./Price";

export default function Product({
  item,
  quantity,
  showImage = true,
  addItem = null,
  removeItem = null,
  showDetails = true
}) {
  return (
    <div className="u-margin-bottom-medium">
      <h4 className="menu__food-title u-margin-bottom-small">{item.name}</h4>

      {showImage && item.type === "food" && showDetails && (
        <Link to={"/menu/" + item.slug}>
          <img src={`${item.image}`} alt={item.alt} className="menu__image" />
        </Link>
      )}

      <div className=" u-margin-bottom-small">
        <Price amount={item.price} />
      </div>

      <div className=" u-margin-bottom-small">
        {removeItem && quantity > 0 && (
          <Button
            caption="-"
            action={() => {
              removeItem(item.id);
            }}
          />
        )}

        {
          // Is there a quantity?
          quantity ? (
            <span className="menu__item-quantity">{`${quantity}`}</span>
          ) : (
            ""
          )
        }

        {addItem && (
          <Button
            caption="+"
            action={() => {
              addItem(item.id);
            }}
          />
        )}
      </div>
    </div>
  );
}

Product.propTypes = {
  item: PropTypes.object.isRequired,
  quantity: PropTypes.number.isRequired,
  addItem: PropTypes.func,
  removeItem: PropTypes.func,
  showDetails: PropTypes.bool,
  showButtons: PropTypes.bool
};
