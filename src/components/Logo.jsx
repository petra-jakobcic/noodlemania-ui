import { Link } from "react-router-dom";

export default function Logo() {
  return (
    <header>
      <Link to="/">
        <img src="/images/logo.jpg" alt="Logo" className="logo-image" />
      </Link>

      <Link to="/" className="link">
        <h1 className="logo">Noodlemania</h1>
      </Link>
    </header>
  );
}
