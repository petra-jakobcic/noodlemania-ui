import { useParams } from "react-router";
import { useContext } from "react";
import { ProductContext } from "../contexts/ProductContext";
import { BasketContext } from "../contexts/BasketContext";
import { Link } from "react-router-dom";
import Page from "./Page";
import Button from "../components/Button";

export default function ProductPage() {
  // Context
  const { products } = useContext(ProductContext);
  const { basket, addItem, removeItem } = useContext(BasketContext);

  // Params
  const { slug } = useParams();

  const item = products.find(item => item.slug === slug);
  const quantity = basket[item.id];

  // If there is no item,
  // show a 'not found' message.
  if (!item) {
    return <p>Product not found.</p>;
  }

  return (
    <Page>
      <h2 className="menu__food-title u-margin-bottom-small">{item.name}</h2>

      <img src={item.image} alt={item.title} width={200} />

      <p>{item.description}</p>

      <h4>Allergy Info</h4>
      <p>This dish contains the following allergens:</p>
      <p className="u-margin-bottom-small">{item.allergens.join(", ")}</p>

      <div className="button-panel u-margin-bottom-small">
        {quantity > 0 && (
          <Button caption="-" action={() => removeItem(item.id)} />
        )}

        {quantity ? <span className="product-quantity">{quantity}</span> : ""}

        <Button caption="+" action={() => addItem(item.id)} />
      </div>

      <Link to="/menu" className="link menu-link">
        &larr; back to the menu
      </Link>
    </Page>
  );
}
