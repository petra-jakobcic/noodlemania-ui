import Logo from "../components/Logo";
import Navigation from "../components/Navigation";

export default function Page(props) {
  return (
    <div className="u-margin-bottom-big">
      <Navigation />
      <Logo />
      {props.children}
    </div>
  );
}
