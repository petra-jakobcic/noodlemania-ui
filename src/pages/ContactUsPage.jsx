import { useState } from "react";
import Page from "./Page";
import InputWithLabel from "../components/InputWithLabel";

export default function ContactUsPage() {
  // State
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("+44 ");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [isError, setIsError] = useState(false);
  const [isNotConnected, setIsNotConnected] = useState(false);
  const [isFormSubmitting, setIsFormSubmitting] = useState(false);

  const handleFormSubmit = e => {
    e.preventDefault();

    setIsFormSubmitting(true);

    fetch("https://noodlemania-api.herokuapp.com/emails", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        firstName,
        lastName,
        phoneNumber,
        email,
        message
      })
    })
      .then(response => {
        if (response.ok) {
          setIsSubmitted(true);
          setIsError(false);
          setIsNotConnected(false);
        } else {
          setIsError(true);
          setIsNotConnected(false);
          setIsFormSubmitting(false);
        }
      })
      .catch(() => {
        setIsNotConnected(true);
        setIsError(false);
        setIsFormSubmitting(false);
      });
  };

  const handleMessageChange = e => setMessage(e.target.value);

  // If the form is submitted,
  // display the 'thank you' message.
  if (isSubmitted) {
    return (
      <Page>
        <p>Thank you for submitting the form!</p>
      </Page>
    );
  }

  return (
    <Page>
      <h1 className="u-margin-bottom-medium">Contact Us</h1>

      {isFormSubmitting && <p>Submitting the form...</p>}

      {isError && <p>Oops! Failed to send. Please try again later.</p>}

      {isNotConnected && (
        <p>
          Oops! It looks like there's a network problem. Please make sure you
          are connected and try again.
        </p>
      )}

      <form onSubmit={handleFormSubmit} className="form">
        <InputWithLabel
          id="first-name"
          label="First Name:"
          value={firstName}
          setValue={setFirstName}
          disabled={isFormSubmitting}
        />
        <InputWithLabel
          id="last-name"
          label="Last Name:"
          value={lastName}
          setValue={setLastName}
          disabled={isFormSubmitting}
        />
        <InputWithLabel
          id="phone"
          label="Your Phone Number:"
          value={phoneNumber}
          setValue={setPhoneNumber}
          disabled={isFormSubmitting}
        />
        <InputWithLabel
          id="email"
          label="Your E-mail Address:"
          value={email}
          setValue={setEmail}
          disabled={isFormSubmitting}
        />
        <div className="u-margin-bottom-medium">
          <label htmlFor="message">Your Message:</label>
          <textarea
            className="form__textarea"
            id="message"
            value={message}
            onChange={handleMessageChange}
            cols="30"
            rows="10"
            placeholder="Type your message here..."
            disabled={isFormSubmitting}
          />
        </div>

        <div className="form__button-wrap">
          <input
            type="submit"
            value="SUBMIT"
            disabled={isFormSubmitting}
            className="button form__button"
          />
        </div>
      </form>
    </Page>
  );
}
