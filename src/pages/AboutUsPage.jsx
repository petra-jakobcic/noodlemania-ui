import Page from "./Page";

export default function AboutUsPage() {
  return (
    <Page>
      <h1 className="u-margin-bottom-medium">About This App</h1>

      <div className="u-margin-bottom-medium">
        This app was created by
        <span className="about__owner">
          <a href="https://petra-jakobcic.herokuapp.com/" className="link">
            Petra Jakobcic
          </a>
        </span>
        using the following tools:
      </div>

      <div className="about__skills">
        <div className="u-margin-bottom-medium">
          <h3 className="u-margin-bottom-small">Back-end</h3>

          <ul className="about__list">
            <li>
              <h4>NodeJS</h4>

              <ul className="about__list">
                <li>
                  NPM packages: express, mongodb, mongoose, dotenv, nodemon,
                  cors
                </li>

                <li>Express</li>

                <li>Headless RESTful API</li>
              </ul>
            </li>
          </ul>
        </div>

        <div className="u-margin-bottom-medium">
          <h3 className="u-margin-bottom-small">Front-end</h3>

          <ul className="about__list">
            <li>
              <h4>React</h4>
              <ul className="about__list">
                <li>React Router DOM</li>
                <li>
                  Hooks: useState, useEffect, useContext, useCallback, useParams
                </li>
                <li>React icons</li>
              </ul>

              <h4>HTML</h4>

              <ul className="about__list">
                <li>Semantic elements</li>
              </ul>
            </li>

            <li>
              <h4>CSS</h4>

              <ul className="about__list">
                <li>Responsive mobile-first design</li>
                <li>Sass</li>
                <li>Flexbox &amp; grid</li>
                <li>Media queries</li>
                <li>
                  <strong>B</strong>lock
                  <strong>E</strong>lement
                  <strong>M</strong>odifier methodology
                </li>
              </ul>
            </li>

            <li>
              <h4>JavaScript</h4>

              <ul className="about__list">
                <li>fetch API</li>
                <li>Local storage</li>
                <li>Pure functions</li>
              </ul>
            </li>
          </ul>
        </div>

        <div>
          <h3 className="u-margin-bottom-small">Workflow</h3>

          <ul className="about__list">
            <li>Visual Studio Code</li>
            <li>Git version control</li>
            <li>Bitbucket</li>
            <li>Trello</li>
            <li>Bash command line</li>
            <li>Markdown for readme files</li>
            <li>Postman HTTP client</li>
            <li>Heroku deployment</li>
          </ul>
        </div>
      </div>
    </Page>
  );
}
