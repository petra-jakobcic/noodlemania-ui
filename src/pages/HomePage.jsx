import { Link } from "react-router-dom";
import Page from "./Page";

export default function HomePage() {
  return (
    <Page>
      <div className="index">
        <p className="u-margin-bottom-big">Feeling manically hungry?</p>

        <div>
          <Link to="/menu" className="button">
            See our menu
          </Link>
        </div>
      </div>
    </Page>
  );
}
