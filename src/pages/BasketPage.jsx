import { useContext, useEffect } from "react";
import { BasketContext } from "../contexts/BasketContext";
import { ProductContext } from "../contexts/ProductContext";
import { Link } from "react-router-dom";
import Page from "./Page";
import Price from "../components/Price";
import Product from "../components/Product";

export default function BasketPage() {
  // Context
  const {
    basket,
    addItem,
    removeItem,
    getBasketItems,
    getBasketSize,
    getBasketTotal,
    emptyBasket
  } = useContext(BasketContext);

  const { fetchProducts } = useContext(ProductContext);

  const filterBasketItemsPerType = type => {
    return getBasketItems().filter(item => item.type === type);
  };

  // Get all the products on component mount.
  useEffect(() => {
    fetchProducts();
  }, [fetchProducts]);

  const foodItems = filterBasketItemsPerType("food");
  const drinkItems = filterBasketItemsPerType("drink");

  const handleEmptyBasketClick = e => {
    emptyBasket();
  };

  return (
    <Page>
      <div className="basket-page">
        <h1 className="u-margin-bottom-medium">Your Basket</h1>

        {getBasketSize() ? (
          <div>
            {foodItems.length > 0 && (
              <>
                <h3 className="menu__heading u-margin-bottom-small">Food</h3>
                {foodItems.map(product => (
                  <Product
                    key={product.id}
                    item={product}
                    quantity={basket[product.id]}
                    addItem={addItem}
                    removeItem={removeItem}
                    showDetails={false}
                  />
                ))}
              </>
            )}

            {drinkItems.length > 0 && (
              <>
                <h3 className="menu__heading u-margin-bottom-small">Drinks</h3>
                {drinkItems.map(product => (
                  <Product
                    key={product.id}
                    item={product}
                    quantity={basket[product.id]}
                    addItem={addItem}
                    removeItem={removeItem}
                    showDetails={false}
                  />
                ))}
              </>
            )}

            <div className="u-margin-bottom-medium">
              Total price: <Price amount={getBasketTotal()} />
            </div>

            <Link to="/checkout">
              <button className="button u-margin-bottom-big">
                go to payment
              </button>
            </Link>

            <p className="u-text-italic u-margin-bottom-small">
              Do you want to empty the basket?
            </p>
            <button className="button" onClick={handleEmptyBasketClick}>
              empty basket
            </button>
          </div>
        ) : (
          <p>Your basket is empty.</p>
        )}
      </div>
    </Page>
  );
}
