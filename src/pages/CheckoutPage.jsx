import { useState, useContext } from "react";
import { BasketContext } from "../contexts/BasketContext";
import { Link } from "react-router-dom";
import Page from "./Page";
import Price from "../components/Price";
import InputWithLabel from "../components/InputWithLabel";

export default function CheckoutPage() {
  // Context
  const { getBasketTotal, emptyBasket } = useContext(BasketContext);

  // State
  const [fullName, setFullName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("+44 ");
  const [email, setEmail] = useState("");
  const [deliveryStreet, setDeliveryStreet] = useState("");
  const [deliveryCity, setDeliveryCity] = useState("");
  const [deliveryPostcode, setDeliveryPostcode] = useState("");
  const [cardholderName, setCardholderName] = useState("");
  const [cardType, setCardType] = useState("visa");
  const [cardNumber, setCardNumber] = useState("");
  const [billingStreet, setBillingStreet] = useState("");
  const [billingCity, setBillingCity] = useState("");
  const [billingPostcode, setBillingPostcode] = useState("");
  const [expiryDate, setExpiryDate] = useState("");
  const [securityCode, setSecurityCode] = useState("");
  const [billingAnswer, setBillingAnswer] = useState("yes");
  const [terms, setTerms] = useState(false);

  const [isSubmitted, setIsSubmitted] = useState(false);
  const [isError, setIsError] = useState(false);
  const [isNotConnected, setIsNotConnected] = useState(false);
  const [isFormSubmitting, setIsFormSubmitting] = useState(false);

  const handleCardTypeChange = e => setCardType(e.target.value);
  const handleExpiryDateChange = e => setExpiryDate(e.target.value);
  const handleBillingAnswerChange = e => setBillingAnswer(e.target.value);
  const handleTermsChange = e => setTerms(e.target.checked);

  const handleFormSubmit = e => {
    e.preventDefault();

    if (!terms) {
      return;
    }

    setIsFormSubmitting(true);

    fetch("https://noodlemania-api.herokuapp.com/payment", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        fullName,
        phoneNumber,
        email,
        deliveryStreet,
        deliveryCity,
        deliveryPostcode,
        cardholderName,
        cardNumber,
        cardType,
        billingStreet,
        billingCity,
        billingPostcode,
        expiryDate,
        securityCode,
        billingAnswer,
        terms
      })
    })
      .then(response => {
        if (response.ok) {
          setIsSubmitted(true);
          setIsError(false);
          setIsNotConnected(false);
          emptyBasket();
        } else {
          setIsError(true);
          setIsNotConnected(false);
          setIsFormSubmitting(false);
        }
      })
      .catch(() => {
        setIsNotConnected(true);
        setIsError(false);
        setIsFormSubmitting(false);
      });
  };

  const showBillingForm = billingAnswer === "no";

  // If the Checkout form is submitted,
  // display the 'Your order has been placed' message.
  if (isSubmitted) {
    return (
      <Page>
        <p>Your payment has been processed!</p>
        <p>Thank you for placing your order!</p>
        <p className="u-margin-bottom-big">Enjoy your meal! :)</p>

        <Link to="/" className="link menu-link">
          &larr; back to Noodlemania
        </Link>
      </Page>
    );
  }

  return (
    <Page>
      <h1>Checkout</h1>

      <div className="u-margin-bottom-medium">
        {/* Total price to pay:{" "} */}
        Total price to pay:
        <span className="total-price">
          <Price amount={getBasketTotal()} />
        </span>
      </div>

      {isFormSubmitting && <p>Submitting the form...</p>}

      {isError && <p>Oops! Failed to send. Please try again later.</p>}

      {isNotConnected && (
        <p>
          Oops! It looks like there's a network problem. Please make sure you
          are connected and try again.
        </p>
      )}

      <form onSubmit={handleFormSubmit} className="form">
        <h3 className="u-text-center u-margin-bottom-small">
          Delivery Information
        </h3>
        <InputWithLabel
          id="full-name"
          label="Full Name:"
          value={fullName}
          setValue={setFullName}
          disabled={isFormSubmitting}
          placeholder="Your Full Name"
        />
        <InputWithLabel
          id="phone-number"
          label="Phone Number:"
          value={phoneNumber}
          setValue={setPhoneNumber}
          disabled={isFormSubmitting}
        />
        <InputWithLabel
          id="email"
          label="E-mail:"
          value={email}
          setValue={setEmail}
          disabled={isFormSubmitting}
          placeholder="e-mail@gmail.com"
        />
        <InputWithLabel
          id="delivery-street"
          label="Street Address:"
          value={deliveryStreet}
          setValue={setDeliveryStreet}
          disabled={isFormSubmitting}
          placeholder="Street No."
        />
        <InputWithLabel
          id="delivery-city"
          label="City:"
          value={deliveryCity}
          setValue={setDeliveryCity}
          disabled={isFormSubmitting}
          placeholder="City Name"
        />
        <InputWithLabel
          id="delivery-postcode"
          label="Postcode:"
          value={deliveryPostcode}
          setValue={setDeliveryPostcode}
          disabled={isFormSubmitting}
          placeholder="Postcode"
        />
        <h3 className="u-text-center u-margin-bottom-small">
          Billing Information
        </h3>
        <InputWithLabel
          id="cardholder-name"
          label="Cardholder Name:"
          value={cardholderName}
          setValue={setCardholderName}
          disabled={isFormSubmitting}
          placeholder="Cardholder Name"
        />
        <div>
          <label htmlFor="card-type">Select the card type:</label>
          <select
            className="form__input"
            name="card-type"
            id="card-type"
            onChange={handleCardTypeChange}
            value={cardType}
          >
            <option value="visa">Visa</option>
            <option value="visa-electron">Visa Electron</option>
            <option value="mastercard">MasterCard</option>
            <option value="maestro">Maestro</option>
          </select>
        </div>
        <InputWithLabel
          id="card-number"
          label="Card Number:"
          value={cardNumber}
          setValue={setCardNumber}
          disabled={isFormSubmitting}
          placeholder="0123 0456 0789 0123"
        />
        <div>
          <label htmlFor="expiry-date">Card Expiry Date:</label>
          <input
            className="form__input date"
            type="date"
            value={expiryDate}
            onChange={handleExpiryDateChange}
            disabled={isFormSubmitting}
          />
        </div>
        <InputWithLabel
          id="security-code"
          label="Card Security Code:"
          value={securityCode}
          setValue={setSecurityCode}
          disabled={isFormSubmitting}
          placeholder="XXX"
        />

        <p className="u-text-italic">
          Is the billing address the same as the delivery address?
        </p>
        <div>
          <span className="option-yes">
            <label htmlFor="yes" className="label-yes">
              Yes
            </label>
            <input
              type="radio"
              name="billing-answer"
              id="yes"
              value="yes"
              checked={billingAnswer === "yes"}
              onChange={handleBillingAnswerChange}
            />
          </span>

          <span>
            <label htmlFor="no" className="label-no">
              No
            </label>
            <input
              type="radio"
              name="billing-answer"
              id="no"
              value="no"
              checked={billingAnswer === "no"}
              onChange={handleBillingAnswerChange}
            />
          </span>
        </div>
        {showBillingForm && (
          <>
            <InputWithLabel
              id="billing-street"
              label="Street Address:"
              value={billingStreet}
              setValue={setBillingStreet}
              disabled={isFormSubmitting}
              placeholder="Street No."
            />
            <InputWithLabel
              id="billing-city"
              label="City:"
              value={billingCity}
              setValue={setBillingCity}
              disabled={isFormSubmitting}
              placeholder="City Name"
            />
            <InputWithLabel
              id="billing-postcode"
              label="Postcode:"
              value={billingPostcode}
              setValue={setBillingPostcode}
              disabled={isFormSubmitting}
              placeholder="Postcode"
            />
          </>
        )}

        <div>
          <input
            className="checkbox-terms"
            type="checkbox"
            id="terms"
            checked={terms}
            onChange={handleTermsChange}
          />
          <label htmlFor="terms">I have read the Terms & Conditions.</label>
        </div>
        <p className="u-margin-bottom-small">
          You have <strong>{terms || "NOT "}</strong>agreed to the Noodlemania
          Terms & Conditions.
        </p>

        {terms && (
          <input
            type="submit"
            value="PAY NOW"
            className="button u-margin-bottom-small"
          />
        )}
      </form>
    </Page>
  );
}
