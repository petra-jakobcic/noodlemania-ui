import { useEffect, useContext } from "react";
import { ProductContext } from "../contexts/ProductContext";
import { BasketContext } from "../contexts/BasketContext";
import { Link } from "react-router-dom";
import Page from "./Page";
import Product from "../components/Product";

export default function MenuPage() {
  // Context
  const { fetchProducts, products, isLoading, isError } =
    useContext(ProductContext);
  const { basket, addItem, removeItem } = useContext(BasketContext);

  const food = products.filter(product => product.type === "food");
  const drinks = products.filter(product => product.type === "drink");

  // Get all the products on component mount.
  useEffect(() => {
    fetchProducts();
  }, [fetchProducts]);

  if (isError) {
    return <p>There is an error!</p>;
  }

  if (isLoading) {
    return <p>Loading...</p>;
  }

  return (
    <Page>
      <div className="menu">
        <h1 className="u-margin-bottom-medium">Menu</h1>
        <h3 className="menu__heading u-margin-bottom-small">Food</h3>
        <p className="u-margin-bottom-small u-text-italic">
          Click on the image to see more info about the dish.
        </p>

        <div className="menu__food u-margin-bottom-medium">
          {food.map(foodItem => (
            <Product
              // The current food item might not exist in the basket,
              // so if we search for it, we will get "undefined".
              // In that case, use 0 for the quantity.
              quantity={basket[foodItem.id] || 0}
              item={foodItem}
              key={foodItem.id}
              addItem={addItem}
              removeItem={removeItem}
            />
          ))}
        </div>

        <h3 className="menu__heading u-margin-bottom-small">Drinks</h3>
        <div className="menu__drinks u-margin-bottom-big">
          {drinks.map(drinkItem => (
            <Product
              quantity={basket[drinkItem.id] || 0}
              item={drinkItem}
              key={drinkItem.id}
              addItem={addItem}
              removeItem={removeItem}
              showDetails={false}
              showImage={false}
            />
          ))}
        </div>

        <Link to="/basket" className="button u-margin-bottom-medium">
          Go to basket
        </Link>
      </div>
    </Page>
  );
}
