import PageRouter from "./routing/PageRouter";
import "./sass/main.scss";

export default function App() {
  return <PageRouter />;
}
