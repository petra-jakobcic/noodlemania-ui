import { useState, createContext, useCallback } from "react";

export const ProductContext = createContext();

export function ProductProvider({ children }) {
  // State
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  /**
   * Use the 'useCallback' hook if you want your function to be defined only
   * once and only ever redefined when a dependency changes.
   * In this case, the dependency array is empty.
   */
  const fetchProducts = useCallback(() => {
    setIsLoading(true);

    fetch("https://noodlemania-api.herokuapp.com/products")
      .then(response => response.json())
      .then(productsArray => {
        setProducts(productsArray);
        setIsError(false);
        setIsLoading(false);
      })
      .catch(() => {
        setIsError(true);
        setIsLoading(false);
      });
  }, []);

  const value = { products, isLoading, isError, fetchProducts };

  return (
    <ProductContext.Provider value={value}>{children}</ProductContext.Provider>
  );
}
