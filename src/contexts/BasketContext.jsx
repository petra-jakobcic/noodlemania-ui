import { useState, useEffect, createContext, useContext } from "react";
import { ProductContext } from "./ProductContext";

export const BasketContext = createContext();

/*
 * Our basket object looks like this:
 * {
 *  "001": 2,
 *  "007": 1,
 *  "012": 3
 * }
 **/

export function BasketProvider({ children }) {
  const [basket, setBasket] = useState({});
  const { products } = useContext(ProductContext);

  /**
   * On the first mount of this provider,
   * it checks the browser's local storage for a basket.
   * If there is a basket, then use it.
   */
  useEffect(() => {
    const basketJson = localStorage.getItem("basket");

    if (basketJson) {
      setBasket(JSON.parse(basketJson));
    }
  }, []);

  /**
   * Every time the basket changes,
   * we save the new version of it to the browser's local storage.
   */
  useEffect(() => {
    localStorage.setItem("basket", JSON.stringify(basket));
  }, [basket]);

  /**
   * Adds a product to the basket.
   *
   * @param {number} id The product id.
   *
   * @returns {void}
   */
  const addItem = id => {
    // If the product is already in the basket,
    // increase its quantity by 1.
    if (id in basket) {
      setBasket({ ...basket, [id]: basket[id] + 1 });
    } else {
      // If there is no such product in the basket,
      // add it and set it to 1.
      setBasket({ ...basket, [id]: 1 });
    }
  };

  /**
   * Removes a product from the basket.
   *
   * @param {number} id The product id.
   *
   * @returns {void}
   */
  const removeItem = id => {
    const quantity = basket[id];

    // If the quantity of the item is 1,
    // remove the item from the basket altogether.
    if (quantity === 1) {
      const myBasket = { ...basket };
      delete myBasket[id];
      setBasket(myBasket);
    } else {
      // Otherwise, reduce the quantity by 1.
      setBasket({
        ...basket,
        [id]: quantity - 1
      });
    }
  };

  /**
   * Gets the basket items.
   *
   * @returns {Array} A list of basket items with full details.
   */
  const getBasketItems = () => {
    if (!products.length) {
      return [];
    }

    const ids = Object.keys(basket);
    return ids.map(id => products.find(product => product.id === id));
  };

  /**
   * Gets the basket size.
   *
   * @returns {number} The basket size.
   */
  const getBasketSize = () => {
    return Object.values(basket).reduce((acc, currVal) => acc + currVal, 0);
  };

  /**
   * Gets the total price of basket items.
   *
   * @returns {number} The basket total.
   */
  const getBasketTotal = () => {
    const basketItems = getBasketItems();

    return basketItems.reduce((acc, currVal) => {
      const quantity = basket[currVal.id];
      return acc + currVal.price * quantity;
    }, 0);
  };

  /**
   * Empties the basket.
   *
   * @returns {void}
   */
  const emptyBasket = () => setBasket({});

  const value = {
    basket,
    addItem,
    removeItem,
    getBasketItems,
    getBasketSize,
    getBasketTotal,
    emptyBasket
  };

  return (
    <BasketContext.Provider value={value}>{children}</BasketContext.Provider>
  );
}
