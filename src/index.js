import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { BasketProvider } from "./contexts/BasketContext";
import { ProductProvider } from "./contexts/ProductContext";

ReactDOM.render(
  <React.StrictMode>
    <ProductProvider>
      <BasketProvider>
        <App />
      </BasketProvider>
    </ProductProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
