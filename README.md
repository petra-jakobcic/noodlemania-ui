# 😸 Noodlemania-UI

This is the front-end user interface for Noodlemania, which depends on the Noodlemania RESTful API.

## Installation

1. Clone the project ([Bitbucket repository is here](https://bitbucket.org/petra-jakobcic/noodlemania-ui/src/master/)).
   ```
   git clone https://bitbucket.org/petra-jakobcic/noodlemania-ui.git
   ```
1. Install the NPM dependencies.
   ```
   npm install
   ```
1. Run the app.
   ```
   npm start
   ```
